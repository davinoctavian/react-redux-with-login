//import axios from "axios";

//const API_URL = "http://localhost:3000/api/auth/";

const register = (username, email, password) => {
  // return axios.post(API_URL + "signup", {
  //   username,
  //   email,
  //   password,
  // });
  let data = {"data" : {
    "success": true,
    "message": "User register in successfully",
    "data": {
        "user": {
            "id": 1,
            "name": username,
            "username": username,
            "password" : password,
            "email": email
        },
    }
  }}
  localStorage.setItem("user", JSON.stringify(data));
  return Promise.resolve(data)
};

const login = (username, password) => {
  // return axios
  //   .post(API_URL + "signin", {
  //     username,
  //     password,
  //   })
  //   .then((response) => {
  //     if (response.data.accessToken) {
  //       localStorage.setItem("user", JSON.stringify(response.data));
  //     }

  //     return response.data;
  //   });
  const dataUser = JSON.parse(localStorage.getItem("user"));
  let dataUsername = ""
  let dataPassword = ""
  if (dataUser) {
    dataUsername = dataUser.data.data.user.username
    dataPassword = dataUser.data.data.user.password
  }
  if (dataUsername == username && dataPassword == password){
    // let dataToken = {"accessToken": "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MiwibmFtZSI6IkNsaWVudCIsImVtYWlsIjoiY2xpZW50QGNsaWNrYXBwcy5jbyIsIm1vYmlsZSI6IjEyMzY1NDc4OSIsImltYWdlIjoiL2RlZmF1bHRfaW1hZ2UucG5nIiwiYWRtaW4iOmZhbHNlLCJpYXQiOjE1NDc5MjU0MzIsImV4cCI6MTU1MDUxNzQzMn0.4Vyjd7BG7v8AFSmGKmIs4VM2FBw3gOLn97Qdf6U4jxU"}
    // const newArry = [
    //   ...dataUser,
    //   dataToken
    // ]
    dataUser["accessToken"] = "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MiwibmFtZSI6IkNsaWVudCIsImVtYWlsIjoiY2xpZW50QGNsaWNrYXBwcy5jbyIsIm1vYmlsZSI6IjEyMzY1NDc4OSIsImltYWdlIjoiL2RlZmF1bHRfaW1hZ2UucG5nIiwiYWRtaW4iOmZhbHNlLCJpYXQiOjE1NDc5MjU0MzIsImV4cCI6MTU1MDUxNzQzMn0.4Vyjd7BG7v8AFSmGKmIs4VM2FBw3gOLn97Qdf6U4jxU"
    dataUser.data["message"] = "User logged in successfully"
    localStorage.setItem("user", JSON.stringify(dataUser))
    return Promise.resolve(dataUser)
  }
  else {
    return Promise.reject(new Error('User not registered'))
  }
};

const logout = () => {
  const dataUser = JSON.parse(localStorage.getItem("user"))
  dataUser["accessToken"] = ""
  localStorage.setItem("user", JSON.stringify(dataUser))
};

export default {
  register,
  login,
  logout,
};