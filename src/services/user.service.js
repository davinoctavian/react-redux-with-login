//import axios from "axios";
//import authHeader from "./auth-header";

//const API_URL = "http://localhost:3000/api/test/";
const dataPosting = JSON.parse(localStorage.getItem("dataPost"));

const getPublicContent = () => {
  //return axios.get(API_URL + "all");
  if (!dataPosting) {
    return Promise.resolve({"data": ""})
  }
  return Promise.resolve(dataPosting)
};

// const getUserBoard = () => {
//   return axios.get(API_URL + "user", { headers: authHeader() });
// };

const setPublicContent = (data) => {
  console.log("g kepanggilll")
  if (!dataPosting) {
    console.log("masasa")
    let arr = []
    let dataContent = {
      id: Math.floor(Math.random() * 1000000),
      title: data.title,
      content : data.content,
      posting: data.posting
    }
    arr.push(dataContent);
    localStorage.setItem("dataPost", JSON.stringify(arr))
    return Promise.resolve(arr)
  } else {
    let foundIndex = dataPosting.findIndex(x => x.id == data.id)
    console.log(foundIndex)
    if (foundIndex > -1) {
      let dataContent = {
        id: dataPosting[foundIndex].id,
        title: data.title,
        content : data.content,
        posting : data.posting
      }
      dataPosting[foundIndex] = dataContent
      localStorage.setItem("dataPost", JSON.stringify(dataPosting))
    }
    else {
      let dataContent = {
        id: Math.floor(Math.random() * 1000000),
        title: data.title,
        content : data.content,
        posting : data.posting
      }
      dataPosting.push(dataContent);
      localStorage.setItem("dataPost", JSON.stringify(dataPosting))
    }
    return Promise.resolve(dataPosting)
  }
}

const setPosting = (id) => {
  let foundIndex = dataPosting.findIndex(x => x.id == id)
  if (foundIndex) {
    let dataContent = {
      id: dataPosting[foundIndex].id,
      title: dataPosting[foundIndex].title,
      content : dataPosting[foundIndex].content,
      posting : dataPosting[foundIndex].posting == 0? 1: 0
    }
    dataPosting[foundIndex] = dataContent
    localStorage.setItem("dataPost", JSON.stringify(dataPosting))
  }
  return Promise.resolve(dataPosting)
}

export default {
  getPublicContent,
  setPublicContent,
  setPosting,
  //getUserBoard,
};