import React, { useState, useEffect } from "react";

import UserService from "../services/user.service";

const Home = () => {
  const [content, setContent] = useState("");

  useEffect(() => {
    UserService.getPublicContent().then(
      (response) => {
        let postingTrue = response.filter(x => x.posting == 1)
        setContent(postingTrue);
      },
      (error) => {
        const _content =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setContent(_content);
      }
    );
  }, []);

  return (
    <div className="container">
      {content && content.length && content.map((listValue, index) => {
        return (
          <div className="content-home" key={index}>
            <div className="title-home-content">{listValue.title}</div>
            <div className="content-home-content">{listValue.content}</div>
          </div>
        )
      })}
    </div>
  );
};

export default Home;