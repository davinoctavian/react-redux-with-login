import React, { useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Textarea from "react-validation/build/textarea";
import Select from "react-validation/build/select";
import CheckButton from "react-validation/build/button";
import UserService from "../services/user.service";

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Posting = () => {
  const form = useRef();
  const checkBtn = useRef();

  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [posting, setPosting] = useState(0);
  const [contents, setContents] = useState([]);
  const [successful, setSuccessful] = useState(false);

  const { message } = useSelector(state => state.message);
  const dispatch = useDispatch();

  useEffect(() => {
    UserService.getPublicContent().then(
      (response) => {
        setContents(response);
      },
      (error) => {
        const _content =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setContents(_content);
      }
    );
  }, []);

  const onChangeTitle = (e) => {
    const title = e.target.value;
    setTitle(title);
  }

  const onChangeContent = (e) => {
    const content = e.target.value;
    setContent(content);
  }

  const onChangePosting = (e) => {
    const posting = e.target.value;
    setPosting(posting);
  }

  const onChangeCheckbox = (e) => {
    const id = e.target.value
    UserService.setPosting(id).then(
      (response) => {
        setContents(response)
        window.location.reload(false)
      },
      (error) => {
        console.log(error)
      }
    )
  }
  const handlePosting = (e) => {
    e.preventDefault();

    setSuccessful(false);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      let datacontent = {"title": title, "content": content, "posting": posting}
      UserService.setPublicContent(datacontent).then(
        (response) => {
          setContents(response)
          alert("Success")
          setSuccessful(true)
          setTitle("")
          setContent("")
          setPosting(0)
        },
        (error) => {
          const _content =
            (error.response && error.response.data) ||
            error.message ||
            error.toString();
  
          setContents(_content);
          setSuccessful(false);
        }
      )
    }
  };

  return (
    <div className="container">
      <div className="container-header">
        <Form onSubmit={handlePosting} ref={form}>
          <div>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <Input
                type="text"
                className="form-control"
                name="title"
                value={title}
                onChange={onChangeTitle}
                validations={[required]}
              />
            </div>

            <div className="form-group">
              <label htmlFor="content">Content</label>
              <Textarea
                className="form-control"
                name="content"
                value={content}
                onChange={onChangeContent}
                validations={[required]}
              />
            </div>

            <div className="form-group">
              <label htmlFor="posting">Posting</label>
              <Select className="form-control" name='posting' value={posting} onChange={onChangePosting}>
                <option value='0'>False</option>
                <option value='1'>True</option>
              </Select>
            </div>
            <div className="form-group">
              <button className="btn btn-primary btn-block">Submit</button>
            </div>
          </div>

          {message && (
            <div className="form-group">
              <div className={ successful ? "alert alert-success" : "alert alert-danger" } role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
      </div>
      <div className="container-content">
        <table id="postingdata">
          <thead>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>Content</th>
              <th>Posting</th>
            </tr>
          </thead>
          <tbody>
            {contents && contents.length && contents.map((listValue, index) => {
              return (
                <tr key={index}>
                  <td>{listValue.id}</td>
                  <td>{listValue.title}</td>
                  <td>{listValue.content}</td>
                  <td><input type="checkbox" value={listValue.id} onChange={onChangeCheckbox} checked={listValue.posting == 1? true : false}/></td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Posting;