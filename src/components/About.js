import React, { useState, useEffect } from "react";

//import UserService from "../services/user.service";

const About = () => {
  //const [content, setContent] = useState("");

  // useEffect(() => {
  //   UserService.getUserBoard().then(
  //     (response) => {
  //       setContent(response.data);
  //     },
  //     (error) => {
  //       const _content =
  //         (error.response &&
  //           error.response.data &&
  //           error.response.data.message) ||
  //         error.message ||
  //         error.toString();

  //       setContent(_content);
  //     }
  //   );
  // }, []);

  return (
    <div className="container">
      <ul>
         <li>Name: Davin Octavian</li>
         <li>Birthdate: 13 october 1992</li>
         <li>Education :
           <ol>
             <li>S1 : Bina Nusantara Jakarta</li>
             <li>SMA: Santo Thomas 1 Medan</li>
             <li>SMP: Santo Thomas 4 Medan</li>
             <li>SD: SDN Negeri 1 Pulau Tello</li>
           </ol>
         </li>
         <li>Experience :
           <ol>
             <li>Global Media Visual : Frontend Engineer</li>
             <li>Qwerty Aplikasi Inovasi : Web Developer</li>
             <li>Soltius Indonesia : Technical Consultant Associate</li>
             <li>Indocyber Global Teknologi : .NET Developer</li>
            </ol>
         </li>
         <li>Skill :
           <ol>
             <li>React</li>
             <li>Angular</li>
             <li>HTML</li>
             <li>CSS</li>
             <li>Javascript</li>
           </ol>
         </li>
      </ul>
    </div>
  );
};

export default About;